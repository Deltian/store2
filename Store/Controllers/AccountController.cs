﻿using Microsoft.AspNet.Identity;
using MongoDB.Driver;
using Store.Models.AccountViewModels;
using Store.Services;
using Store.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class AccountController : Controller
    {
        
        private readonly IStoreRepository _storeRepository;

        public AccountController(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository ?? throw new ArgumentNullException(nameof(storeRepository));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            var isLogged = _storeRepository.LoginUser(model);
            
            if (!isLogged)
                return new HttpStatusCodeResult(422);

            return RedirectToAction("GetLoginPanel", new { returnUrl });
        }

        [AjaxOnly]
        public ActionResult GetLoginPanel(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return PartialView("_LoginPartial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(422, ModelState.ToString());
            }

            var isRegistered = _storeRepository.RegisterUser(model);

            if (!isRegistered)
                return new HttpStatusCodeResult(422, "Username and/or email address are already taken");


            return RedirectToAction("MyAccount");
        }

        [Authorize(Roles ="Buyer,Seller")]
        public ActionResult MyAccount(MyAccountCard card = MyAccountCard.MyAccount, bool success = false)
        {
            if (card == MyAccountCard.MyAccount)
            {

                ViewBag.ChildAction = "GetMyAccountData";
                ViewBag.SuccessMessage = success ? "Zaktualizowano twoje dane osobowe" : null;
            }
            else if (card == MyAccountCard.MyBasicData)
            {
                ViewBag.ChildAction = "GetMyBasicData";
                ViewBag.SuccessMessage = success ? "Zaktualizowano twoje podstawowe dane" : null;
            }
            else
            {
                ViewBag.ChildAction = "GetMyPassword";
                ViewBag.SuccessMessage = success ? "Zaktualizowano twoje hasło" : null;
            }

            ViewBag.CurrentCard = card;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateMyAccount(MyAccountViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ChildAction = "GetMyAccountData";
                ViewBag.CurrentCard = MyAccountCard.MyAccount;
                return View("MyAccount");
            }

            _storeRepository.UpdateMyAccountData(model, User.Identity.GetUserId());

            return RedirectToAction("MyAccount", new { card = MyAccountCard.MyAccount, success=true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateMyBasicData(MyBasicDataViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ChildAction = "GetMyBasicData";
                ViewBag.CurrentCard = MyAccountCard.MyBasicData;
                return View("MyAccount");
            }

            var isUserUpdated = _storeRepository.UpdateMyBasicData(model, User.Identity.GetUserId());

            if (!isUserUpdated)
            {
                ViewBag.ChildAction = "GetMyBasicData";
                ViewBag.CurrentCard = MyAccountCard.MyBasicData;
                ViewBag.ErrorMessage= "Podany adres email posiada inny użytkownik";

                return View("MyAccount");
            }

            return RedirectToAction("MyAccount", new { card = MyAccountCard.MyBasicData, success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(MyPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ChildAction = "GetMyPassword";
                ViewBag.CurrentCard = MyAccountCard.MyPassword;
                return View("MyAccount");
            }

            var isPasswordUpdated = _storeRepository.UpdatePassword(model, User.Identity.GetUserId());

            if (!isPasswordUpdated)
            {
                ViewBag.ChildAction = "GetMyPassword";
                ViewBag.CurrentCard = MyAccountCard.MyPassword;
                ViewBag.ErrorMessage= "Hasło, które posiadasz, nie pasuje do tego, które podałeś";

                return View("MyAccount");
            }

            return RedirectToAction("MyAccount", new { card = MyAccountCard.MyPassword, success = true });
        }

        [AjaxOnly]
        public ActionResult ValidateUsername(string username)
        {
            var isUsernameTaken = _storeRepository.GetUserByField(u => u.Username == username);

            return Json(!isUsernameTaken, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult ValidateEmail(string email)
        {
            var isEmailTaken = _storeRepository.GetUserByField(u => u.Email == email);

            return Json(!isEmailTaken, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            _storeRepository.LogOutUser();

            return RedirectToAction("Index", "Home");
        }

        [AjaxOrChildAction]
        public ActionResult GetMyAccountData()
        {
            var model = _storeRepository.GetMyAccountData(User.Identity.GetUserId());

            return PartialView("_EditMyAccountDataPartial", model);
        }

        [AjaxOrChildAction]
        public ActionResult GetMyBasicData()
        {
            var model = _storeRepository.GetMyBasicData(User.Identity.GetUserId());

            return PartialView("_EditMyBasicDataPartial", model);
        }

        [AjaxOrChildAction]
        public ActionResult GetMyPassword()
        {
            return PartialView("_EditMyPasswordPartial");
        }
    }

    public enum MyAccountCard
    {
        MyAccount,
        MyBasicData,
        MyPassword
    }
}