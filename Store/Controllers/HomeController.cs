﻿using Store.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStoreRepository _storeRepository;

        public HomeController(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository ?? throw new ArgumentNullException(nameof(storeRepository));

           
        }

        public ActionResult Index(bool openLoginDialog = false)
        {
            ViewBag.TopCategories = _storeRepository.GetTopCategories();

            ViewBag.OpenLoginDialog = openLoginDialog;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ChildActionOnly]
        public ActionResult CreateDropDownListForCategory()
        {
            var model = _storeRepository.GetTopCategories();

            return PartialView("_DropDownListForCategoryPartial", model);
        }
    }
}