﻿using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using Store.Models.StoreViewModels;
using Store.Services;
using Store.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;

namespace Store.Controllers
{
    public class StoreController : Controller
    {
        private readonly IStoreRepository _storeRepository;

        public StoreController(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        
        public ActionResult Index(string category,string query)
        {
            var model = _storeRepository.GetProductsBy(category, query);

            ViewBag.Category = category;

            return View(model);
        }

        public ActionResult ViewProduct(Guid id)
        {
            var product = _storeRepository.GetProduct(id);

            var model = AutoMapper.Mapper.Map<ViewProductViewModel>(product);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProceedToPurchase(Guid productId, string option, int count)
        {
            if (count < 1)
                throw new HttpException(400, "Quantity of selected products cannot be less than 1");

            _storeRepository.ValidateProductCount(productId, count);

            if (option == "Dodaj do koszyka")
            {
                var productsInCart = (List<ProductInCart>)Session["productsInCart"];

                if (productsInCart == null)
                    productsInCart= new List<ProductInCart>();

                productsInCart.Add(new ProductInCart(productId, count));

                Session.Add("productsInCart", productsInCart);

                return RedirectToAction("ShoppingCart", "Store");
            }
            else if (option == "Kup teraz")
            {
                TempData["productId"] = productId;
                TempData["selectedCount"] = count;

                return RedirectToAction("PurchaseSummary");
            }

            throw new HttpException(400, $"Invalid purchase option '{option}'.");

        }

        public ActionResult ShoppingCart()
        {
            var cart = (List<ProductInCart>)Session["productsInCart"];

            var productsInCart = cart != null ? _storeRepository.GetProductsByCartItems(cart) : null;

            return View(productsInCart);
        }

        public JsonResult ChangeCountOfProducts(Guid productId, int count)
        {
            if (count < 1)
                throw new HttpException(400, "Quantity of selected products cannot be less than 1");

            _storeRepository.ValidateProductCount(productId, count);

                var cart = (List<ProductInCart>)Session["productsInCart"];

                if (cart == null)
                    return Json(new { errorMessage = "Sesja wygasła, koszyk jest pusty." }, JsonRequestBehavior.AllowGet);

                var cartItem = cart.SingleOrDefault(item => item.ProductId == productId);

                if (cartItem == null)
                    throw new HttpException(422, $"There is no such item");

                cartItem.SelectedCount = count;

                var totalQuantity = cart.Sum(item => item.SelectedCount);

                Session["productsInCart"] = cart;

                var totalPricePerItem = _storeRepository.CalculateTotalPriceForProduct(count, productId);

                var formattedTotalPricePerItem = HtmlUtils.FormatNumberIntoPrice(totalPricePerItem);

                return Json(new { totalQuantity, productId, totalPricePerItem = formattedTotalPricePerItem, totalPrice = 0 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveItemFromCart(Guid productId)
        {
            var productExists = _storeRepository.CheckProductExistsById(productId);

            if (!productExists)
                return Json(new {
                    errorMessage = "Produkt, który jest chcesz usunąć, nie istnieje ani w bazie danych, ani w twoim koszyku"
                }, JsonRequestBehavior.AllowGet);

            var cart = (List<ProductInCart>)Session["productsInCart"];

            if (cart == null)
                return Json(new { errorMessage = "Sesja wygasła, koszyk jest pusty." }, JsonRequestBehavior.AllowGet);

            var productInCart = cart.FirstOrDefault(i => i.ProductId == productId);

            if (productInCart == null)
                return Json(new
                {
                    errorMessage = "Produkt, który jest chcesz usunąć, nie znajduje się w twoim koszyku"
                }, JsonRequestBehavior.AllowGet);

            cart.Remove(productInCart);

            Session["productsInCart"] = cart.Count > 0 ? cart : null;

            var totalQuantity = cart.Sum(i => i.SelectedCount);

            return Json(new { totalQuantity }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult PurchaseSummary()
        {
            //if (TempData["productId"] == null && TempData["selectedCount"] == null)
            //{
            //    if (Session["productsInCart"] == null)
            //        throw new HttpException(400, "Session for the shopping cart has expired");

            //    throw new HttpException(400, "There is not product id and/or selected count to display purchase summary");
            //}


            IEnumerable<ProductInCart> cartItems = null;

            if (TempData["productId"] != null && TempData["selectedCount"] != null)
            {

                var productId = (Guid)TempData["productId"];

                var selectedCount = (int)TempData["selectedCount"];

                var cartItem = new ProductInCart(productId, selectedCount);

                cartItems = new List<ProductInCart> { cartItem };
            }
            else if (Session["productsInCart"] != null)
            {
                cartItems = (List<ProductInCart>)Session["productsInCart"];
            }
            TempData.Keep("selectedCount");
            TempData.Keep("productId");

            ViewBag.SelectedProducts = _storeRepository.GetProductsForPurchaseSummary(cartItems);

            ViewBag.TotalPrice = 0;

            var selectedProducts = cartItems.Select(i => new SelectedProductViewModel(i.ProductId, i.SelectedCount)).ToList();

            var model = new PurchaseSummaryViewModel
            {
                Products = selectedProducts
            };

            return View(model);
        }

        public ActionResult SaveProducts(PurchaseSummaryViewModel model)
        {
            var productsFromCart = model.Products.Select(p => new ProductInCart(p.ProductId, p.SelectedCount));

            var products = _storeRepository.GetProductsForPurchaseSummary(productsFromCart);
        }

        public ActionResult Categories(string category)
        {
            ViewBag.Category = category;

            var model = _storeRepository.GetChildrenOfCategory(category);

            return View(model);
        }

        [Authorize(Roles ="Seller")]
        public ActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProduct(AddProductViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            TempData["product"] = model;

            return RedirectToAction("ProductSummary");
        }

        [Authorize(Roles = "Seller")]
        public ActionResult ProductSummary()
        {
            if (TempData["product"] == null)
                return RedirectToAction("AddProduct");

            var model = AutoMapper.Mapper.Map<ProductSummaryViewModel>(TempData["product"]);

            TempData.Keep("product");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProductForSale(ProductSummaryViewModel model)
        {
            var productId = _storeRepository.AddProduct(model);

            return RedirectToAction("ViewProduct", new { id=productId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public PartialViewResult UploadImage(HttpPostedFileBase image)
        {
            ViewBag.ImageId = _storeRepository.AddImage(image);

            ViewBag.IncludeHiddenInput = true;

            return PartialView("_ImageItemPartial");
        }

        public FileResult GetImage(string id)
        {
            var imageStream = _storeRepository.GetImage(id);

            var contentType = imageStream.FileInfo.Metadata["contentType"].AsString;

            return File(imageStream, contentType);
        }

        [ChildActionOnly]
        public ActionResult GetCategoriesHierarchy(string category)
        {
            var model = _storeRepository.GetCategoriesHierarchyFrom(category);

            return PartialView(model);
        }

        public ActionResult GetCountOfProductsInCart()
        {
            var cart = ((List<ProductInCart>)Session["productsInCart"]);

            if (cart != null && cart.Count > 0)
            {
                ViewBag.CountOfItemsInCart = cart.Sum(p => p.SelectedCount);
            }
            else
            {
                ViewBag.CountOfItemsInCart = 0;
            }

            return PartialView("_CountOfItemsInCartPartial");
        }
    }
    public class ProductInCart
    {
        public ProductInCart(Guid productId, int selectedCount)
        {
            ProductId = productId;
            SelectedCount = selectedCount;
        }

        public Guid ProductId { get; set; }

        public int SelectedCount { get; set; }
    }
}