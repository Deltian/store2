﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using Store.Controllers;
using Store.Models.AccountViewModels;
using Store.Models.DomainModels;
using Store.Models.StoreViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Web;

namespace Store.Services
{
    public class StoreRepository : IStoreRepository
    {
        private readonly IMongoDatabase _database;

        private readonly OwinAuthenticationService _owinAuthenticationService;

        private IMongoCollection<T> GetCollection<T>(string collectionName)
            => _database.GetCollection<T>(collectionName);

        public StoreRepository()
        {
            
            var client = new MongoClient();
            _database = client.GetDatabase("Store");
            
            _owinAuthenticationService = new OwinAuthenticationService();
        }

        private User GetUser(string id)
        {
            var objectId = new ObjectId(id);

            var user = GetCollection<User>("Users").Find(u => u.Id == objectId).FirstOrDefault();

            return user;
        }

        private string HashPassword(string password)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                var bytes = System.Text.Encoding.ASCII.GetBytes(password);

                var hashBytes = shaM.ComputeHash(bytes);

                return BitConverter.ToString(hashBytes);
            }
        }

        private Product FindProduct(Guid id)
        {
            var product = GetCollection<Product>("Products").Find(u => u.Id == id).FirstOrDefault();

            return product;
        }

        public bool RegisterUser(RegisterViewModel model)
        {
            var user = Mapper.Map<User>(model);

            user.Roles = new[] { "Buyer" };

                user.PasswordHash = HashPassword(model.Password);

            var count = GetCollection<User>("Users").Find(u => u.Email == model.Email || u.Username == model.Username).CountDocuments();

            if (count > 0)
                return false;

            GetCollection<User>("Users").InsertOne(user);

            _owinAuthenticationService.SignIn(user, true, user.Roles);

            return true;
        }

        public bool LoginUser(LoginViewModel model)
        {
            var passwordHash = HashPassword(model.LoginPassword);

            var user = GetCollection<User>("Users").Find(u => u.Username == model.LoginUsername).FirstOrDefault();

            if (user != null)
            {
                if (user.PasswordHash == passwordHash)
                    _owinAuthenticationService.SignIn(user, model.RememberMe, user.Roles);
                return true;
            }
            return false;
        }

        public void LogOutUser()
        {
            _owinAuthenticationService.SignOut();
        }

        public bool GetUserByField(Expression<Func<User, bool>> predicate)
        {
            return GetCollection<User>("Users").Find(predicate).Any();
        }

        public MyAccountViewModel GetMyAccountData(string id)
        {
            var user = GetUser(id);

            var viewModel = Mapper.Map<MyAccountViewModel>(user);

            Mapper.Map(user.Address, viewModel);

            return viewModel;
        }

        public MyBasicDataViewModel GetMyBasicData(string id)
        {
            var user = GetUser(id);

            var viewModel = Mapper.Map<MyBasicDataViewModel>(user);

            return viewModel;
        }

        public bool UpdatePassword(MyPasswordViewModel model, string id)
        {
            var user = GetUser(id);

            var oldPasswordHash = HashPassword(model.Password);

            if (user.PasswordHash != oldPasswordHash)
                return false;

            var newPasswordHash = HashPassword(model.NewPassword);

            var update = Builders<User>.Update.Set(u => u.PasswordHash, newPasswordHash);

            GetCollection<User>("Users").UpdateOne(u => u.Id == new ObjectId(id), update);

            return true;
        }

        public bool UpdateMyBasicData(MyBasicDataViewModel model, string id)
        {
            var count = GetCollection<User>("Users").Find(u => u.Email == model.Email).CountDocuments();

            if (count > 0)
                return false;

            var update = Builders<User>.Update.Set(u => u.Email, model.Email);

            GetCollection<User>("Users").UpdateOne(u => u.Id == new ObjectId(id), update);

            return true;
        }

        public void UpdateMyAccountData(MyAccountViewModel model, string id)
        {
            var address = Mapper.Map<Address>(model);

            var update = Builders<User>.Update
                .Set(u => u.Address, address)
                .Set(u => u.BirthDate, model.BirthDate)
                .Set(u => u.Company, model.Company).
                Set(u => u.FirstName, model.FirstName)
                .Set(u => u.LastName, model.LastName)
                .Set(u => u.PhoneNumber, model.PhoneNumber);

            var objectId = new ObjectId(id);

            GetCollection<User>("Users").UpdateOne(u => u.Id == objectId, update);
        }

        public IEnumerable<string> GetTopCategories()
        {
            var categories= GetCollection<ProductCategory>("Categories").Find(c => c.Parent == "root").Project(c => c.Name).ToList();

            return categories;
        }

        public ChildrenOfCategoryViewModel GetChildrenOfCategory(string category)
        {
            var childrenCategories = GetCollection<ProductCategory>("Categories")
                .Find(c => c.Tree.Count() ==3 && c.Tree.ElementAt(1) == category).ToList();

            if (!childrenCategories.Any())
            {
                throw new HttpException(422,"Could not found the given category in the categories collection");
            }

            var model = new ChildrenOfCategoryViewModel
            {
                CategoryGroups = childrenCategories.GroupBy(c => c.Parent).Select(g => new CategoryGroup(g.Key, g.Select(c => c.Name)))
            };

            return model;
        }

        public IEnumerable<string> GetCategoriesHierarchyFrom(string category)
        {
            if (string.IsNullOrEmpty(category) || string.IsNullOrWhiteSpace(category))
            {
                category = "root";
            }

            var categoryEntity = GetCollection<ProductCategory>("Categories").Find(c => c.Name == category).First();

            var list = new List<string>();

            list.AddRange(categoryEntity.Tree);
            list.Add(categoryEntity.Name);

            return list;
        }

        public IEnumerable<ProductItemViewModel> GetProductsBy(string category, string query)
        {
            if (string.IsNullOrEmpty(category) || string.IsNullOrWhiteSpace(category))
            {
                category = "root";
            }

            var productCategoriesCount = GetCollection<ProductCategory>("Categories").CountDocuments(c => c.Name == category);

            if (productCategoriesCount == 0)
            {
                throw new HttpException(422, "Could not found the given category in the categories collection");
            }

            var productsQuery = GetCollection<Product>("Products").Aggregate().Match(p => p.CategoriesHierarchy.Contains(category));

            IEnumerable<Product> products = null;

            if (!string.IsNullOrEmpty(query) && !string.IsNullOrWhiteSpace(query))
            {
                products = productsQuery.Match(p => p.Name.Contains(query)).ToList();
            }
            else
            {
                products = productsQuery.ToList();
            }

            return Mapper.Map<IEnumerable<ProductItemViewModel>>(products);
        }

        public Product GetProduct(Guid id)
        {
            var product = FindProduct(id);

            if (product == null)
                throw new HttpException(404, $"Cannot find product of id '{id}'.");

            return product;
        }

        public void ValidateProductCount(Guid id, int selectedCount)
        {
            var productsCursor = GetCollection<Product>("Products").Find(u => u.Id == id);

            if (!productsCursor.Any())
                throw new HttpException(404, $"Cannot find product with id {id}.");

            var productCount = productsCursor.Project(p => p.Count).First();

            if (productCount < selectedCount)
                throw new HttpException(400, $"Selected count of products cannot be greater than available count of products.");
        }

        public Guid AddProduct(ProductSummaryViewModel model)
        {
            var product = Mapper.Map<Product>(model);

            product.Id = Guid.NewGuid();

            GetCollection<Product>("Products").InsertOne(product);

            return product.Id;
        }

        public bool CheckProductExistsById(Guid id)
        {

            var productExists = GetCollection<Product>("Products").Find(p => p.Id == id).Any();

            return productExists;
        }

        public ShoppingCartViewModel GetProductsByCartItems(IEnumerable<ProductInCart> cart, int? pageIndex = null, int? pageSize = null)
        {
            var filter = Builders<Product>.Filter.In(x => x.Id, cart.Select(p => p.ProductId));
            
            var products = GetCollection<Product>("Products").Find(filter).Skip(pageIndex).Limit(pageSize).Project(p => new ProductInCartViewModel
            {
                CategoriesHierarchy = p.CategoriesHierarchy,
                AvailableCount = p.Count,
                Id = p.Id,
                ImageId = p.ImageIds.ToArray()[0],
                Name = p.Name,
                Price = p.Price,
            }).ToList();

            var totalPrice = GetCollection<Product>("Products").AsQueryable().Where(p => filter.Inject()).Sum(p => cart.ToArray().First(p2 => p2.ProductId == p.Id).SelectedCount * p.Count);
            foreach (var productInCart in cart)
            {
                var product = products.First(p => p.Id == productInCart.ProductId);
                product.SelectedCount = productInCart.SelectedCount;

                product.TotalPrice = product.SelectedCount * product.Price;
            }

            var shoppingCart = new ShoppingCartViewModel(products, totalPrice);

            return shoppingCart;
        }

        public IEnumerable<PurchaseItemViewModel> GetProductsForPurchaseSummary(IEnumerable<ProductInCart> cart, int? pageIndex = null, int? pageSize = null)
        {
            var filter = Builders<Product>.Filter.In(x => x.Id, cart.Select(p => p.ProductId));

            var products = GetCollection<Product>("Products").Find(filter).Skip(pageIndex).Limit(pageSize).Project(p =>
            new PurchaseItemViewModel
            {
                Id = p.Id,
                ImageId = p.ImageIds.ToArray()[0],
                Name = p.Name,
                Price = p.Price,
                SelectedCount = 1,
                
            }).ToList();

            return products;
        }

        public decimal CalculateTotalPriceForProduct(int count, Guid productId)
        {
            var price = GetCollection<Product>("Products").Find(p => p.Id == productId).Project(p => p.Price).First();

            return count * price;
        }

        public ObjectId AddImage(HttpPostedFileBase file)
        {
            var bucket = new GridFSBucket(_database);

            var options = new GridFSUploadOptions { Metadata = new BsonDocument("contentType", file.ContentType) };

            var imageId = bucket.UploadFromStream(file.FileName, file.InputStream, options);

            return imageId;
        }

        public GridFSDownloadStream GetImage(string id)
        {
            var bucket = new GridFSBucket(_database);

            var objectId = new ObjectId(id);

            var stream = bucket.OpenDownloadStream(objectId);

            return stream;
        }
    }
}