﻿using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using Store.Controllers;
using Store.Models.AccountViewModels;
using Store.Models.DomainModels;
using Store.Models.StoreViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Store.Services
{
    public interface IStoreRepository
    {
        bool RegisterUser(RegisterViewModel model);
        bool LoginUser(LoginViewModel model);
        void LogOutUser();
        bool GetUserByField(Expression<Func<User, bool>> predicate);
        MyAccountViewModel GetMyAccountData(string id);
        MyBasicDataViewModel GetMyBasicData(string id);
        bool UpdatePassword(MyPasswordViewModel model, string id);
        bool UpdateMyBasicData(MyBasicDataViewModel model, string id);
        void UpdateMyAccountData(MyAccountViewModel model, string id);

        IEnumerable<string> GetTopCategories();
        IEnumerable<string> GetCategoriesHierarchyFrom(string category);
        ChildrenOfCategoryViewModel GetChildrenOfCategory(string category);

        IEnumerable<ProductItemViewModel> GetProductsBy(string category, string query);
        Product GetProduct(Guid id);
        Guid AddProduct(ProductSummaryViewModel model);
        void ValidateProductCount(Guid id, int selectedCount);
        bool CheckProductExistsById(Guid id);

        decimal CalculateTotalPriceForProduct(int count, Guid productId);
        ShoppingCartViewModel GetProductsByCartItems(IEnumerable<ProductInCart> cart, int? pageIndex = null, int? pageSize = null);

        IEnumerable<PurchaseItemViewModel> GetProductsForPurchaseSummary(IEnumerable<ProductInCart> cart, int? pageIndex = null, int? pageSize = null);

        ObjectId AddImage(HttpPostedFileBase file);
        GridFSDownloadStream GetImage(string id);
    }
}
