﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Utils
{
    public static class DropDownLists
    {
        public static IEnumerable<SelectListItem> GetShippingMethods()
        {
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "wybierz", Value = "0" });
            list.Add(new SelectListItem { Text = "list polecony", Value = "1" });
            list.Add(new SelectListItem { Text = "Kurier", Value = "2" });
            list.Add(new SelectListItem { Text = "Kurier za pobraniem", Value = "3" });

            return list;
        }

        public static IEnumerable<SelectListItem> CreateDropDownListFrom(IEnumerable<string> elements)
        {
            var selectList = elements.Select(e => new SelectListItem { Text=e, Value=e }).ToList();

            selectList.Insert(0, new SelectListItem { Text = "wszystkie kategorie", Value = "root" });

            return selectList;
        }

        public static IEnumerable<SelectListItem> Voivodeships()
        {
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "wybierz", Value = "0" });
            list.Add(new SelectListItem { Text = "Dolnośląskie", Value = "1" });
            list.Add(new SelectListItem { Text = "Podkarpackie", Value = "2" });
            list.Add(new SelectListItem { Text = "Wielkopolskie", Value = "3" });
            list.Add(new SelectListItem { Text = "Małopolskie", Value = "4" });
            list.Add(new SelectListItem { Text = "Kujawsko-pomorskie", Value = "5" });
            list.Add(new SelectListItem { Text = "Warmińsko-mazurskie", Value = "6" });

            return list;
        }
    }

    public class HtmlUtils
    {
        public static string FormatNumberIntoPrice(decimal price)
        {
            var formattedPrice = price.ToString("C", CultureInfo.CreateSpecificCulture("pl-PL"));

            return formattedPrice;
        }
    }
}