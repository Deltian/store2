﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Store.Utils
{
    public class AjaxOrChildActionAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        => controllerContext.HttpContext.Request.IsAjaxRequest() || controllerContext.IsChildAction;
    }
}