﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.ValidationAttributes
{
    public class PasswordAttribute : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule();

            validationRule.ErrorMessage = FormatErrorMessage(metadata.DisplayName);
            validationRule.ValidationType = "passwordcheck";

            yield return validationRule;
        }

        public override bool IsValid(object value)
        {
            var password = value.ToString();

            bool hasLowerCaseLetter = password.Any(c => char.IsLower(c));

            bool hasUpperCaseLetter = password.Any(c => char.IsUpper(c));

            bool hasDigit = password.Any(c => char.IsDigit(c));

            bool hasSpecialChars = password.Any(c => !char.IsLetterOrDigit(c));

            return hasLowerCaseLetter && hasUpperCaseLetter && hasDigit && hasSpecialChars;
        }
    }
}