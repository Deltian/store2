﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.ValidationAttributes
{
    public class ValidateCheckboxesAttribute : ValidationAttribute, IClientValidatable
    {
        public ValidateCheckboxesAttribute(string otherProperty)
        {
            OtherProperty = otherProperty;
        }

        public string OtherProperty { get; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var validationRule = new ModelClientValidationRule();

            validationRule.ErrorMessage = ErrorMessageString;
            validationRule.ValidationType = "validatecheckboxes";

            validationRule.ValidationParameters["otherproperty"] = OtherProperty;

            yield return validationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var modelType = validationContext.ObjectType;
            var prop = modelType.GetProperty(OtherProperty);

            var otherPropValue = (bool)prop.GetValue(validationContext.ObjectInstance);

            if ((bool)value && otherPropValue)
                return ValidationResult.Success;

            return new ValidationResult(ErrorMessageString ?? ErrorMessage);
        }
    }
}