﻿$("#uploadImage").change(function () {
    var image = this.files[0];

    var formData = new FormData();

    formData.append("image", image);

    var csrfToken = $("[name='__RequestVerificationToken']").val();
    formData.append("__RequestVerificationToken", csrfToken);

    $.ajax({
        method: 'post',
        data: formData,
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        url: "/Store/UploadImage",
        success: function (view) {
            $("#images").append(view);
        }
    });
});