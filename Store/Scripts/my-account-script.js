﻿$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $("#myAccountMenu").on("click", "li", function () {
        if (!$(this).children("a").hasClass("active")) {
            $(this).siblings().each(function () { $(this).children("a").removeClass("active"); });

            $(this).children("a").addClass("active");

            var url = $(this).data("url");

            $.get(url, function (response) {
                $("#myAccountBody").html(response);
            });
        }
    });

    var errorMessage = $("#myAccountScript").attr("error-message");

    var successMessage = $("#myAccountScript").attr("success-message");

    if (errorMessage) {
        var $errorAlertTemplate = $('#errorAlertTemplate').contents().clone();

        $errorAlertTemplate.insertBefore('.container-fluid');

        var $errorAlert = $("#errorAlert");

        $errorAlert.children("span").html(errorMessage);

        $errorAlert.addClass("show");

        setTimeout(function () {
            $errorAlert.alert("close");
        }, 4000);
    }

    if (successMessage) {
        var $successAlertTemplate = $('#successAlertTemplate').contents().clone();

        $successAlertTemplate.insertBefore('.container-fluid');

        var $successAlert = $("#successAlert");

        $successAlert.children("span").html(successMessage);

        $successAlert.addClass("show");

        setTimeout(function () {
            $successAlert.alert("close");
        }, 4000);
    }
});
