﻿$(".alert").alert();

var $successAlertTemplate = $("#successAlertTemplate").contents().clone();

var $errorAlertTemplate = $('#errorAlertTemplate').contents().clone();

var $loginModal = $('#loginModal');

var showAlert = false;

function showErrorAlert(jqXHR, status, err) {
    $errorAlertTemplate.insertBefore('.container');

    var $errorAlert = $("#errorAlert");

    if (jqXHR.status == 422) {
        $errorAlert.children("span").html(serverResources.invalidUsernameOrPassword);

        
    }
    else {
        $errorAlert.children("span").text(serverResources.serverError);
    }

    $errorAlert.addClass("show");

    setTimeout(function () {
        $errorAlert.alert("close");
    }, 4000);
        
}

function onLoginSuccess(data, status, jqXHR) {

    $loginModal.modal('hide');

    showAlert = true;
    

    setTimeout(function () {
        $("#successAlert").alert("close");
    }, 4000);
}

$loginModal.on("hidden.bs.modal", function () {
    if (showAlert) {
        $successAlertTemplate.insertBefore('.container');
        $("#successAlert").children("span").text(serverResources.loginSucceded);
        $("#successAlert").addClass("show");
    }


    showAlert = false;
});

$(".modal").each(function () {
    $(this).on("hidden.bs.modal", function () {
        $(this).find("form")[0].reset();
    });
});

$loginModal.on("hide.bs.modal", function () {
    $("#errorAlert").alert("close");
});

$.validator.addMethod("passwordcheck", function (value, element, params) {
    var regex = new RegExp(params);

    return regex.test(value);
});

$.validator.unobtrusive.adapters.add("passwordcheck", [], function (options) {
    options.rules["passwordcheck"] = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])";
    options.messages["passwordcheck"] = options.message;
});

$.validator.addMethod("validatecheckboxes", function (value, element, params) {
    var otherCheckboxValue = $(params).is(":checked");

    return otherCheckboxValue && value == "true";
});

$.validator.unobtrusive.adapters.add("validatecheckboxes", ["otherproperty"], function (options) {
    options.rules["validatecheckboxes"] = "#" + options.params.otherproperty;
    options.messages["validatecheckboxes"] = options.message;
});