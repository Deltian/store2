﻿$("#shoppingCart").on("change", ".count-input", function () {
    var changedCount = $(this).val();

    $.post("/Store/ChangeCountOfProducts", { count: changedCount, productId: $(this).prev().val() }, function (json) {

        if (json.errorMessage)
            showErrorAlert(json.errorMessage);
        else {

            var productItem = $("#" + json.productId);


            productItem.find(".total-price").text(json.totalPricePerItem);

            $("#totalPrice").text(json.totalPrice);

            $(".product-count-badge").text(json.totalQuantity);
        }

    });
});

$("#shoppingCart").on("click", ".remove-cart-item", function () {

    var currentItem = $(this).closest("li");

    var productId = currentItem.attr("id");

    $.post("/Store/RemoveItemFromCart", { productId }, function (json) {
        if (json.errorMessage)
            showErrorAlert(json.errorMessage);
        else {
            if (json.totalQuantity)
                $(".product-count-badge").text(json.totalQuantity);
            else
                $(".product-count-badge").remove();

            currentItem.remove();

            var countOfItems = $("#shoppingCart>li").length;

            if (!countOfItems) {
                $("<h3>Koszyk jest pusty</h3>").insertAfter("#shoppingCart");

                $("#shoppingCartFooter").remove();
            }
            
        }
    });
});

function showErrorAlert(errorMessage) {
    var $errorAlertTemplate = $('#errorAlertTemplate').contents().clone();

    $errorAlertTemplate.insertBefore('.container');

    var $errorAlert = $("#errorAlert");

    $errorAlert.children("span").html(errorMessage);

    $errorAlert.addClass("show");

    setTimeout(function () {
        $errorAlert.alert("close");
    }, 4000);
}