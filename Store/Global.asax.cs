﻿using AutoMapper;
using Store.App_Start;
using Store.Models.AccountViewModels;
using Store.Models.DomainModels;
using Store.Models.StoreViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Store
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<RegisterViewModel, User>(MemberList.None);
                cfg.CreateMap<User, MyAccountViewModel>(MemberList.None);
                cfg.CreateMap<MyAccountViewModel, Address>(MemberList.Destination);
                cfg.CreateMap<Address, MyAccountViewModel>(MemberList.Source);

                cfg.CreateMap<Product, ProductItemViewModel>(MemberList.Destination);
                cfg.CreateMap<Product, ViewProductViewModel>(MemberList.Destination);
                cfg.CreateMap<AddProductViewModel, ProductSummaryViewModel>(MemberList.Destination);
                cfg.CreateMap<ProductSummaryViewModel, Product>(MemberList.Source);
                cfg.CreateMap<Product, PurchaseItemViewModel>(MemberList.None).AfterMap((s,d) => d.ImageId = s.ImageIds.ElementAt(0));
            });

            Mapper.AssertConfigurationIsValid();

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Sid;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IocConfigurator.ConfigureDependencyInjection();
            DatabaseInitializer.InitializeDatabase();
        }
    }
}
