﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.DomainModels
{
    public class User
    {
        public User()
        {
            BoughtProducts = new List<BoughtProduct>();
        }

        [BsonId]
        public ObjectId Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string[] Roles { get; set; }

        public string Company { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public Address Address { get; set; }

        public IList<BoughtProduct> BoughtProducts { get; set; }
    }

    public class BoughtProduct
    {
        public Guid ProductId { get; set; }

        public int SelectedCount { get; set; }
    }

    public class Address
    {
        public string Street { get; set; }

        public string AptNumber { get; set; }

        public int? FlatNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int Voivodeship { get; set; }
    }
}