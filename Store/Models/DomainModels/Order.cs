﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.DomainModels
{
    public class Order
    {
        public IEnumerable<SavedProduct> Products { get; set; }

        public decimal TotalPrice { get; set; }

        public string ShippingMethod { get; set; }

        public string Message { get; set; }

        public string RecipientFirstName { get; set; }

        public string RecipientLastName { get; set; }

        public Address Address { get; set; }

        public bool Finalized { get; set; }
    }
}