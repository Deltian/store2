﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.DomainModels
{
    public class ProductCategory
    {
        public ProductCategory(string name, string parent = null, params string[] tree)
        {
            Name = name;
            Parent = parent;
            Tree = tree;
        }

        [BsonId]
        public string Name { get; set; }

        [BsonIgnoreIfNull]
        public string Parent { get; set; }

        [BsonIgnoreIfNull]
        public string[] Tree { get; set; }
    }
}