﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.DomainModels
{
    public class Product
    {
        [BsonId]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public IEnumerable<string> CategoriesHierarchy { get; set; }

        public IEnumerable<string> ImageIds { get; set; }
    }
}