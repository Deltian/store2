﻿using Store.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class AddProductViewModel
    {

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.ProductName))]
        [StringLength(30, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.StringLength))]
        public string Name { get; set; }

        public string[] ImageIds { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Display(ResourceType = typeof(DisplayNames), Name = "CountOfProducts")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "NumberRange")]
        public int Count { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Display(ResourceType = typeof(DisplayNames), Name = "Price")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "NumberRange")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Display(ResourceType = typeof(DisplayNames), Name = "Description")]
        public string Description { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = "Categories")]
        public string[] CategoriesHierarchy { get; set; }
    }
}