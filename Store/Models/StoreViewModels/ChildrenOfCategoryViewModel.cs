﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class ChildrenOfCategoryViewModel
    {

        public IEnumerable<CategoryGroup> CategoryGroups { get; set; }

    }
}