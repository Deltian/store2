﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class FinalizePurchaseViewModel
    {
        public IList<SelectedProductViewModel> Products { get; set; }

        public int ShippingMethod { get; set; }

        public string Message { get; set; }
    }
}