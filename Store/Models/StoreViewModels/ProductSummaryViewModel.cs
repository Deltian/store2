﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Models.StoreViewModels
{
    public class ProductSummaryViewModel
    {

        [HiddenInput(DisplayValue = false)]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:0,0}")]
        [HiddenInput(DisplayValue = false)]
        public int Count { get; set; }

        [HiddenInput(DisplayValue = false)]
        public decimal Price { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Description { get; set; }

        public string[] ImageIds { get; set; }

        public string[] CategoriesHierarchy { get; set; }
    }
}