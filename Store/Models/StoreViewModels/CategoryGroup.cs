﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class CategoryGroup
    {
        public CategoryGroup(string upperCategory, IEnumerable<string> lowerCategories)
        {
            UpperCategory = upperCategory;
            LowerCategories = lowerCategories;
        }

        public string UpperCategory { get; set; }

        public IEnumerable<string> LowerCategories { get; set; }
    }
}