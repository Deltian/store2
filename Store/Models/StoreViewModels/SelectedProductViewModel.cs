﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class SelectedProductViewModel
    {
        public SelectedProductViewModel(Guid productId, int selectedCount)
        {
            ProductId = productId;
            SelectedCount = selectedCount;
        }

        public Guid ProductId { get; set; }

        public int SelectedCount { get; set; }
    }
}