﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class PurchaseItemViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int SelectedCount { get; set; }

        public string ImageId { get; set; }

        public decimal TotalPrice => SelectedCount * Price;
    }
}