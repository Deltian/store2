﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class ProductInCartViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int AvailableCount { get; set; }

        public int SelectedCount { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice { get; set; }

        public IEnumerable<string> CategoriesHierarchy { get; set; }

        public string ImageId { get; set; }
    }
}