﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Store.Resources;

namespace Store.Models.StoreViewModels
{
    public class PurchaseSummaryViewModel
    {
        public IList<SelectedProductViewModel> Products { get; set; }

        [Display(ResourceType =typeof(DisplayNames), Name = "ShippingMethod")]
        [Range(1,5, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        public int ShippingMethod { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = "Message")]
        public string Message { get; set; }
    }
}