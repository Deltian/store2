﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models.StoreViewModels
{
    public class ShoppingCartViewModel
    {
        public ShoppingCartViewModel(IEnumerable<ProductInCartViewModel> products, decimal totalPrice)
        {
            Products = products;
            TotalPrice = totalPrice;
        }
        public IEnumerable<ProductInCartViewModel> Products { get; set; }

        public decimal TotalPrice { get; set; }
    }
}