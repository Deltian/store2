﻿using Store.Resources;
using Store.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Display(Name = "Username", ResourceType = typeof(DisplayNames))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [StringLength(12, MinimumLength = 5, ErrorMessageResourceName = "StringLength", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Remote("ValidateUsername", "Account", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName =nameof(ValidationMessages.UsernameAlreadyExists))]
        public string Username { get; set; }

        [Display(Name = "Email", ResourceType = typeof(DisplayNames))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [EmailAddress(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName ="Email")]
        [Remote("ValidateEmail", "Account", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.EmailAddressIsTaken))]
        public string Email { get; set; }

        [Display(Name = "Password", ResourceType = typeof(DisplayNames))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Password(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Password")]
        [StringLength(12, MinimumLength = 5, ErrorMessageResourceName = "StringLength", ErrorMessageResourceType = typeof(ValidationMessages))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(DisplayNames))]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Compare")]
        public string ConfirmPassword { get; set; }

        [ValidateCheckboxes(nameof(OtherRulesToAccept),ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "AllChecked")]
        public bool RulesToAccept { get; set; }

        public bool OtherRulesToAccept { get; set; }
    }
}