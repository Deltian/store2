﻿using Store.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store.Models.AccountViewModels
{
    public class MyAccountViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.FirstName))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.LastName))]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.Company))]
        public string Company { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.Street))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        public string Street { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.HouseNumber))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        public string AptNumber { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.FlatNumber))]
        public int? FlatNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [RegularExpression(@"^[0-9]{2}\-[0-9]{3}$")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.PostalCode))]
        public string PostalCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.City))]
        public string City { get; set; }

        [Range(1, 6, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.Voivodeship))]
        public int Voivodeship { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Phone(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = nameof(ValidationMessages.PhoneNumber))]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.PhoneNumber))]
        public string PhoneNumber { get; set; }

        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.BirthDate))]
        public DateTime? BirthDate { get; set; }
    }
}