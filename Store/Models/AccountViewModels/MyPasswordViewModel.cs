﻿using Store.Resources;
using Store.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store.Models.AccountViewModels
{
    public class MyPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = "Password")]
        public string Password { get; set; }

        [Password(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Password")]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = "NewPassword")]
        [StringLength(12, MinimumLength = 5, ErrorMessageResourceName = "StringLength", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string NewPassword { get; set; }

        [Compare(nameof(NewPassword), ErrorMessageResourceName = "Compare", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}