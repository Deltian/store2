﻿using Store.Resources;
using Store.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Display(Name = "Username", ResourceType = typeof(DisplayNames))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName ="Required")]
        public string LoginUsername { get; set; }

        [Display(Name = "Password", ResourceType = typeof(DisplayNames))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")] 
        public string LoginPassword { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(DisplayNames))]
        public bool RememberMe { get; set; }
    }
}