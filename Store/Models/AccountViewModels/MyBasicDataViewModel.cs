﻿using Store.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Store.Models.AccountViewModels
{
    public class MyBasicDataViewModel
    {
        [ReadOnly(true)]
        [Display(ResourceType = typeof(DisplayNames), Name =nameof(DisplayNames.Username))]
        public string Username { get; set; }

        [EmailAddress(ErrorMessageResourceType =typeof(ValidationMessages), ErrorMessageResourceName = nameof(DisplayNames.Email))]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(DisplayNames), Name = nameof(DisplayNames.Email))]
        public string Email { get; set; }
    }
}