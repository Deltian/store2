﻿using MongoDB.Driver;
using Store.Models.DomainModels;
using Store.Resources;
using Store.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;

namespace Store.App_Start
{
    public class DatabaseInitializer
    {
        public static void InitializeDatabase()
        {
            var client = new MongoClient();
            var database = client.GetDatabase("Store");


            var categoriesCollection = database.GetCollection<ProductCategory>("Categories");

            var count = categoriesCollection.CountDocuments(_ => true);

            var thirdLevelCategories = new List<ProductCategory>();

            if (count == 0)
            {
                var root= new ProductCategory("root");

                var firstLevelCategoryNames = new[] { "Electronics", "Fashion", "House", "Supermarket", "Child", "Beauty", "Health", "Entertainment", "Sport", "AutomotiveIndustry", "Announces", "CollectionsAndArt", "Company" };

                var firstLevelCategories = new List<ProductCategory>();

                foreach (var category in firstLevelCategoryNames)
                {
                    firstLevelCategories.Add(new ProductCategory(category, root.Name, root.Name));
                }

                var secondLevelCategories = new List<ProductCategory>();

                var secondLevelCategoryNames = new[] { "1stC", "2ndC", "3rdC", "4thC", "5thC", "6thC" };

                foreach (var category in firstLevelCategories)
                {
                    foreach (var categoryName in secondLevelCategoryNames)
                    {
                        secondLevelCategories.Add(new ProductCategory(category.Name+"_"+categoryName, category.Name, root.Name, category.Name));
                    }
                    
                }
                
                var thirdLevelCategoryNames = new[] { "1stC", "2ndC", "3rdC", "4thC", "5thC", "6thC" };

                

                foreach (var category in secondLevelCategories)
                {
                    foreach (var categoryName in thirdLevelCategoryNames)
                    {
                        thirdLevelCategories.Add(new ProductCategory(category.Name +"_"+ categoryName, category.Name, root.Name, category.Parent, category.Name));
                    }
                }

                var allCategories = new List<ProductCategory> { root };

                allCategories.AddRange(firstLevelCategories);

                allCategories.AddRange(secondLevelCategories);

                allCategories.AddRange(thirdLevelCategories);

                categoriesCollection.InsertMany(allCategories);
            }

            var productsCollection = database.GetCollection<Product>("Products");

            var count2 = productsCollection.CountDocuments(_ => true);

            if (count2 == 0)
            {
                var category = database.GetCollection<ProductCategory>("Categories").Find(c => c.Name == "Electronics_1stC_1stC").First();

                var categoriesHierarchy = new List<string>();
                categoriesHierarchy.AddRange(category.Tree);
                categoriesHierarchy.Add(category.Name);

                productsCollection.InsertOne(new Product { Name = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Count = 20, Price = 3444.99m, Description = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Category = "Electronics_1stC_1stC", CategoriesHierarchy= categoriesHierarchy, ImageIds=new[] { "5bcaf01b59f5680ba05eb2c9" } });
                productsCollection.InsertOne(new Product { Name = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Count = 20, Price = 3444.99m, Description = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Category = "Electronics_1stC_1stC", CategoriesHierarchy = categoriesHierarchy, ImageIds = new[] { "5bcaf01b59f5680ba05eb2c9" } });
                productsCollection.InsertOne(new Product { Name = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Count = 20, Price = 3444.99m, Description = "Lenovo FLEX 5 i7-7500U 16GB SSD512GB 940MX 2GB W10", Category = "Electronics_1stC_1stC", CategoriesHierarchy = categoriesHierarchy, ImageIds = new[] { "5bcaf01b59f5680ba05eb2c9" } });
            }
        }
    }
}